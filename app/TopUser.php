<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TopUser extends Model
{
    protected $fillable = ['user_id', 'votes'];

    public $timestamps = false;

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public static function getTop()
    {
        return static::with('user')->orderBy('votes', 'desc')->take(3)->get();
    }
}
