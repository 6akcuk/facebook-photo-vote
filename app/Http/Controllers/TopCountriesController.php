<?php

namespace App\Http\Controllers;

use App\TopCountry;
use Illuminate\Http\Request;

class TopCountriesController extends Controller
{
    public function index()
    {
        return TopCountry::getTop();
    }
}
