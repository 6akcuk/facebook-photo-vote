<?php

namespace App\Http\Controllers;

use App\User;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class UsersController extends Controller
{
    /**
     * Authorise facebook user in the app.
     * 
     * @param  LaravelFacebookSdk $fb [description]
     * @return [type]                 [description]
     */
    public function facebook(LaravelFacebookSdk $fb)
    {
        try {
            $token = $fb->getJavaScriptHelper()->getAccessToken();
        } catch (FacebookSDKException $e) {
            \Log::debug('Failed to obtain access token.', [$e->getMessage()]);

            abort(500, 'Failed to obtain access token.');
        }

        session()->put('facebook_access_token', (string) $token);
        $fb->setDefaultAccessToken($token);

        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,first_name,last_name,email,birthday,hometown,location');
        } catch (FacebookSDKException $e) {
            \Log::debug('Cannot get basic info from Facebook.', [$e->getMessage()]);

            abort(500, 'Cannot get basic info from Facebook.');
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();

        // User is not created
        $user = User::firstOrNew([
            'facebook_user_id' => $facebook_user->getId(),
            'email' => $facebook_user->getEmail(),
        ]);

        if ( ! $user->exists) {
            $user->password = bcrypt(rand(0, 10000));
        }

        $user->name = $facebook_user->getName();
        $user->firstname = $facebook_user->getFirstName();
        $user->lastname = $facebook_user->getLastName();

        // Update birthday
        if ($facebook_user->getBirthday() instanceof DateTime) {
            $user->birthday = $facebook_user->getBirthday()->format('Y-m-d');
        }

        // Update hometown, homecountry
        $hometown = $facebook_user->getHometown()->getName();
        if ($hometown && stristr($hometown, ',') !== false) {
            list($town, $country) = explode(', ', $hometown);

            $user->hometown = $town;
            $user->homecountry = $country;
        }

        // Update current location
        $location = $facebook_user->getLocation()->getName();
        if ($location && stristr($location, ',') !== false) {
            list($town, $country) = explode(', ', $location);

            $user->current_town = $town;
            $user->current_country = $country;
        }

        // Get user picture.
        try {
            $response = $fb->get('/me/picture?type=normal&redirect=false');
        } catch (FacebookSDKException $e) {
            \Log::debug('Cannot get user picture from Facebook.', [$e->getMessage()]);

            abort(500, 'Cannot get user picture from Facebook.');
        }

        $graph = $response->getGraphNode();
        $user->photo = $graph->getField('url');

        $user->save();

        // Log the user into Laravel
        auth()->login($user);

        return array_merge($user->toArray(), ['votes' => $user->votes()->count()]);
    }
}
