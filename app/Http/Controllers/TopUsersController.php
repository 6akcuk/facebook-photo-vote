<?php

namespace App\Http\Controllers;

use App\TopUser;
use Illuminate\Http\Request;

class TopUsersController extends Controller
{
    public function index()
    {
        return TopUser::getTop();
    }
}
