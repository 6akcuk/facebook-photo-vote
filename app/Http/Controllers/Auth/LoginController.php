<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\SocialAccountService;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Create a redirect method to facebook api.
     * 
     * @return void
     */
    public function redirect() 
    {
        return Socialite::driver('facebook')
            ->scopes(['user_birthday', 'user_hometown', 'user_photos'])
            ->redirect();
    }

    /**
     * Return a callback method to facebook api.
     * 
     * @return callback URL from facebook
     */
    public function callback(LaravelFacebookSdk $fb) 
    {
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Access token will be null if the user denied the request
        // or if someone just hit this URL outside of the OAuth flow.
        if ( ! $token) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if ( ! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

        // Save for later
        \Session::put('fb_user_access_token', (string) $token);

        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,first_name,last_name,email,birthday,hometown,location');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();

        $user = User::firstOrNew(['email' => $facebook_user->getEmail()]);

        $user->facebook_user_id = $facebook_user->getId();
        $user->name = $facebook_user->getName();
        $user->firstname = $facebook_user->getFirstName();
        $user->lastname = $facebook_user->getLastName();
        $user->password = bcrypt(rand(0, 10000));
        $user->access_token = $token;

        // Update birthday
        $user->birthday = $facebook_user->getBirthday()->format('Y-m-d');

        // Update hometown, homecountry
        $hometown = $facebook_user->getHometown()->getName();
        list($town, $country) = explode(', ', $hometown);

        $user->hometown = $town;
        $user->homecountry = $country;

        // Update current location
        $location = $facebook_user->getLocation()->getName();
        list($town, $country) = explode(', ', $hometown);

        $user->current_town = $town;
        $user->current_country = $country;

        // Get user picture.
        try {
            $response = $fb->get('/me/picture?type=normal&redirect=false');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        $graph = $response->getGraphNode();

        $user->photo = $graph->getField('url');

        $user->save();

        // Log the user into Laravel
        \Auth::login($user);

        return redirect('/')->with('message', 'Successfully logged in with Facebook');
    }
}
