<?php

namespace App\Http\Controllers;

use App\Album;
use App\Jobs\DownloadPhotos;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AlbumsController extends Controller
{
    /**
     * List all user's albums and group by approved status.
     *
     * @return Collection
     */
    public function index() 
    {
        return Album::with(['photos' => function ($query) {
                return $query->withCount('votes');
            }])
            ->where('user_id', auth()->user()->id)
            ->get()
            ->groupBy(function ($album, $key) {
                return $album->approved ? 'approved' : 'pending';
            });
    }

    /**
     * Create new user's album.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $album = auth()->user()->albums()->create();

        dispatch(new DownloadPhotos($album, $request->photos, $request->accessToken));

        return response()->json(['status' => 'ok']);
    }

    /**
     * Get random album for voting.
     *
     * @param Request $request
     * @return mixed
     */
    public function random(Request $request)
    {
        return Album::with('photos', 'user')
            ->has('photos', '=', 3)
            ->when($request->has('notId'), function ($query) use ($request) {
                return $query->where('id', '!=', $request->notId);
            })
            ->when(auth()->check(), function ($query) {
                return $query->where('user_id', '!=', auth()->user()->id)
                    ->whereNotExists(function ($query) {
                        return $query->select(DB::raw(1))
                            ->from('votes')
                            ->join('photos', 'photos.id', '=', 'votes.photo_id')
                            ->where('votes.user_id', auth()->user()->id)
                            ->whereRaw('photos.album_id = albums.id');
                    });
            })
            ->approved()
            ->inRandomOrder()
            ->take($request->input('next', "false") == "true" ? 1 : 2)
            ->get();
    }
}
