<?php

namespace App\Http\Controllers\Photos;

use App\Events\TopCountriesUpdated;
use App\Events\TopUsersUpdated;
use App\Events\UserVoted;
use App\Events\UserVotedOn;
use App\Http\Controllers\Controller;
use App\Photo;
use App\TopCountry;
use App\TopUser;
use Illuminate\Http\Request;

class VotesController extends Controller
{
    public function store(Photo $photo, Request $request)
    {
        // Don't create a new vote if already voted on
        $hasVote = $photo->votes()->where('user_id', auth()->user()->id)->count();

        if ($hasVote) {
            return;
        }

        $photo->votes()->create([
            'user_id' => auth()->user()->id,
            'country' => auth()->user()->homecountry ?: ''
        ]);

        // Broadcast votes
        broadcast(new UserVoted(auth()->user()));
        broadcast(new UserVotedOn($photo->album->user));

        // Increase top statistic
        TopUser::where('user_id', auth()->user()->id)->increment('votes');

        if (auth()->user()->homecountry) {
            TopCountry::where('country', auth()->user()->homecountry)->increment('votes');

            broadcast(new TopCountriesUpdated(TopCountry::getTop()->toArray()));
        }

        // Broadcast top lists
        broadcast(new TopUsersUpdated(TopUser::getTop()->toArray()));
    }
}