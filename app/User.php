<?php

namespace App;

use App\Album;
use App\Photo;
use App\TopUser;
use App\Vote;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_user_id', 'firstname', 'lastname', 'birthdate',
        'homecountry', 'hometown', 'current_country', 'current_town'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'access_token'
    ];

    protected $appends = ['voted_on_me'];

    public function albums() 
    {
        return $this->hasMany(Album::class);
    }

    public function photos() 
    {
        return $this->hasManyThrough(Photo::class, Album::class);
    }

    public function votes() 
    {
        return $this->hasMany(Vote::class, 'user_id');
    }

    /**
     * Get builder to fetch all peoples who voted on me.
     *
     * @return mixed
     */
    public function votedOnMe()
    {
        return Vote::whereHas('photo', function ($query) {
            return $query->whereHas('album', function ($query) {
                return $query->where('user_id', $this->id);
            });
        });
    }

    /**
     * User top (total) votes. Used for top table.
     */
    public function top() 
    {
        return $this->hasOne(TopUser::class);
    }

    /**
     * Count total received votes.
     *
     * @return mixed
     */
    public function getVotedOnMeAttribute()
    {
        return $this->votedOnMe()->count();
    }
}
