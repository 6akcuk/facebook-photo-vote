<?php

namespace App\Providers;

use App\TopCountry;
use App\TopUser;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function ($user) {
            // Create top user record
            $user->top()->create(['votes' => 0]);

            // Create top country record
            if ($user->homecountry) {
                TopCountry::firstOrCreate(['country' => $user->homecountry]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
