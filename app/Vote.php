<?php

namespace App;

use App\Photo;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = ['user_id', 'country'];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function photo() 
    {
        return $this->belongsTo(Photo::class);
    }
}
