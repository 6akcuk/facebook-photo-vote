<?php

namespace App;

use App\Photo;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function photos() 
    {
        return $this->hasMany(Photo::class);
    }

    public function scopeApproved($query)
    {
        return $query->where('approved', true);
    }
}
