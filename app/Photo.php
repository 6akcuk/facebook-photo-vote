<?php

namespace App;

use App\Album;
use App\Vote;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['facebook_photo_id', 'path'];

    public function album() 
    {
        return $this->belongsTo(Album::class);
    }

    public function votes() 
    {
        return $this->hasMany(Vote::class);
    }
}
