<?php

namespace App\Jobs;

use App\Album;
use Facebook\Exceptions\FacebookSDKException;
use Folklore\Image\Facades\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Http\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class DownloadPhotos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Album.
     *
     * @var Album
     */
    public $album;

    /**
     * List of photos.
     *
     * @var array
     */
    public $photos;

    /**
     * Facebook access token.
     *
     * @var string
     */
    public $accessToken;

    /**
     * Create a new job instance.
     *
     * @param Album $album
     * @param array $photos
     * @param string $accessToken
     * @return void
     */
    public function __construct(Album $album, array $photos, string $accessToken)
    {
        $this->album = $album;
        $this->photos = $photos;
        $this->accessToken = $accessToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $storage = Storage::disk('public');
        /** @var LaravelFacebookSdk $fb */
        $fb = resolve(LaravelFacebookSdk::class);

        foreach ($this->photos as $photo) {
            // Retrieve thumbnails from Facebook
            try {
                $response = $fb->get('/'. $photo['id'] .'?fields=images', $this->accessToken);
            } catch (FacebookSDKException $e) {
                dd($e->getMessage());
            }

            // Get only thumbnail with height 320
            $facebookPhoto = $response->getGraphNode();
            $images = $facebookPhoto->getField('images')->asArray();

            $image = $this->findNearestImage($images);
            if ( ! $image) {
                $image = $this->findNearestImage($images, 100);
            }

            // Prepare save data
            if ( ! $image['source']) {
                \Log::debug('Image does not have appropriate size.', $images);

                abort(500, 'Image does not have appropriate size.');
            }

            $contents = file_get_contents($image['source']);
            $extension = preg_replace("/.*\.([A-z]*)\?oh.*/", "$1", $image['source']);
            $path = sprintf('%s/%s.%s', date('Y/m/d'), sha1($image['source']), $extension);

            if ( ! $storage->put($path, $contents)) {
                $this->album->delete();

                abort(500, 'Cannot save image');
            }

            $this->album->photos()->create([
                'facebook_photo_id' => $photo['id'],
                'path' => $path
            ]);
        }
    }

    protected function findNearestImage($images, $size = 300) {
        if ($size > 700) {
            return null;
        }

        foreach ($images as $image) {
            if ($image['height'] >= $size && $image['height'] <= $size + 100) {
                return $image;
            }
        }

        return $this->findNearestImage($images, $size + 100);
    }
}
