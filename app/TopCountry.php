<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopCountry extends Model
{
    protected $fillable = ['country', 'votes'];

    public $timestamps = false;

    public static function getTop()
    {
        return static::orderBy('votes', 'desc')->take(3)->get();
    }
}
