<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/redirect', 'Auth\LoginController@redirect');
Route::get('/callback', 'Auth\LoginController@callback');

Route::put('/users/facebook', 'UsersController@facebook');

// Top
Route::get('/top-users.json', 'TopUsersController@index');
Route::get('/top-countries.json', 'TopCountriesController@index');

// Albums
Route::get('albums/random', 'AlbumsController@random');

Route::middleware(['auth'])->group(function () {
    // Albums
    Route::resource('albums', 'AlbumsController');

    // Photos
    Route::resource('photos.votes', 'Photos\VotesController');
    Route::resource('photos', 'PhotosController');
});