#!/usr/bin/env bash

# Call the script with --config to replace .env file with .env.example

if ! [ -z ${1+x} ];
then
	if [ $1 == "--config" ]
	then
		cp -rf ./.env.example ./.env
	fi
fi

cd ./laradock/

rm .env
cp env-example .env

docker-compose down

docker-compose up -d --build nginx mysql

docker-compose exec mysql bash -c "mysql -uroot -proot < docker-entrypoint-initdb.d/createdb.sql.example"

docker-compose exec --user=laradock workspace bash -c "rm composer.lock; rm -rf vendor; composer install && php artisan key:generate && php artisan migrate && php artisan db:seed; rm -rf node_modules; npm i && npm run dev"
