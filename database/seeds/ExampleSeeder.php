<?php

use Illuminate\Database\Seeder;

class ExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 5 new users
        for ($i = 1; $i <= 5; $i++) {
            $user = factory('App\User')->create();

            for ($k = 1; $k <= 5; $k++) {
                $album = factory('App\Album')->create(['user_id' => $user->id]);

                for ($p = 1; $p <= 3; $p++) {
                    factory('App\Photo')->create(['album_id' => $album->id]);
                }
            }
        }
    }
}
