<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialiteFacebookFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('facebook_user_id')->unsigned()->index();
            $table->string('access_token')->nullable();
            $table->string('photo')->nullable();
            $table->string('firstname', 50);
            $table->string('lastname', 50);
            $table->string('birthday', 10)->nullable();
            $table->string('homecountry', 40)->nullable();
            $table->string('hometown', 100)->nullable();
            $table->string('current_country', 40)->nullable();
            $table->string('current_town', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex(['facebook_user_id']);

            $table->dropColumn(['facebook_user_id', 'access_token', 'firstname', 'lastname', 'birthday', 'homecountry', 'hometown', 'current_country', 'current_town']);
        });
    }
}
