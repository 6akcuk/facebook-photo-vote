<?php

use Faker\Generator as Faker;

$factory->define(App\Vote::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'photo_id' => function () {
            return factory('App\Photo')->create()->id;
        },
        'country' => $faker->country
    ];
});
