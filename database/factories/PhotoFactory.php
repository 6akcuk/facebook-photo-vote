<?php

use Faker\Generator as Faker;

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'album_id' => function () {
            return factory('App\Album')->create()->id;
        },
        'facebook_photo_id' => 0,
        'path' => $faker->imageUrl(262, 383, null, false)
    ];
});
