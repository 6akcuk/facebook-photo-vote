#!/usr/bin/env bash

# Call the script with --config to replace .env file with .env.example

cd ./laradock/

docker-compose up -d nginx mysql
