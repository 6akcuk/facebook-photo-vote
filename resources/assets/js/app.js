
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import App from './containers/App.vue';
import notification from 'notification-js';

Vue.prototype.$eventHub = new Vue();

const app = new Vue({
    el: '#app',

    components: { App },

    created() {
        this.$eventHub.$on('axios-error', error => {
            if (error.response) {
                notification.notify('error', error.response.data.message || error.response.data.errors);

                return;
            }

            console.dir(error);

            notification.notify('error', error);
        });

        this.$eventHub.$on('notify-error', error => notification.notify('error', error));
    }
});