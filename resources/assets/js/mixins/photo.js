export default {
    methods: {
        url(path) {
            return path.substr(0, 4) == 'http' ? path : '/storage/' + path;
        }
    }
}