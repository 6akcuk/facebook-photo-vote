export default {
    data() {
        return {
            topList: []
        }
    },

    methods: {
        load() {
            axios.get(this.url)
                .then(response => {
                    this.topList = response.data
                })
                .catch(error => this.$eventHub.$emit('axios-error', error));
        }
    },

    created() {
        this.load();
    }
}